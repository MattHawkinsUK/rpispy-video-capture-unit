#!/usr/bin/python
import os
import time
import shutil
import subprocess

# Function definitions
def DetectNetwork(interface='eth0'):
  # Read ifconfig.txt and determine
  # if network is connected
  
  try:
    # Get network details using ifconfig
    filename = '/home/pi/ifconfig_' + interface + '.txt'
    f = open(filename, "w")
    subprocess.call(["/sbin/ifconfig", interface], stdout=f)      
    f.close()
    
    # Check network details to see if interface exists
    f = open(filename, 'r')
    line = f.readline() # skip 1st line
    line = f.readline() # read 2nd line
    f.close()
    
    if line.find('inet addr:')>0:
      return True
    else:
      return False
  except:
    return False

if os.path.exists('/boot/vcu_config.py'):
  # Config file exists in boot partition
  shutil.copyfile('/boot/vcu_config.py','/home/pi/rpispy_vcu/config.py')    
    
if os.path.exists('/home/pi/rpispy_vcu/config.py'):    
  # Run main script
  import config

  # Short delay to give network time to connect
  time.sleep(2)
  
  network=DetectNetwork('eth0')
  
  # Run? | LAN | Flag
  #-------------------
  #   1  |  0  |  1  |
  #   1  |  0  |  0  |
  #   1  |  1  |  1  |
  #   0  |  1  |  0  |

  if (network==False) or (network==True and config.AUTORUN_IF_LAN==True):
    subprocess.Popen("sudo python /home/pi/rpispy_vcu/vcu.py 1", shell=True)