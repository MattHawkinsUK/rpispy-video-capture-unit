       ___  ___  _ ____          
      / _ \/ _ \(_) __/__  __ __ 
     / , _/ ___/ /\ \/ _ \/ // / 
    /_/|_/_/  /_/___/ .__/\_, /  
                   /_/   /___/   

    RPiSpy Video Capture Unit
    Installation Instructions
    
    
Hardware Setup
--------------

You will need a Raspberry Pi with a camera module attached. I used a PiNoIR in my tests but the standard
camera module will work just as well. The camera should be enabled using the raspi-config utility.

The script also assumes there is an LED (with suitable resistor) on GPIO4 and a switch on GPIO7.


Script Install
--------------
    
Prepare a fresh SD card with the latest Raspbian image.

Power up the Pi and login.

You will now be in the home directory. Run the following commands ending each line with an [ENTER] key.

mkdir rpispy_vcu
cd rpispy_vcu
mkdir videos

Now we can download the archive file from BitBucket using :

  wget https://bitbucket.org/MattHawkinsUK/rpispy-video-capture-unit/get/master.tar.gz

If you type ls you should see the master.tar.gz file sitting in the current directory. Use the following command to extract the files from the archive :

  tar -xvf master.tar.gz --strip 1

Typing ls should show you a list of the files :

* cron.py        - file called on boot
* vcu.py         - main Python script
* config.py      - configuration settings
* prepare_mp4.sh - convert all h264 files to MP4
* master.tar.gz  - downloaded archive

You can remove the archive now using :

  rm master.tar.gz


Config File Setup
-----------------

If you want to be able to edit the config file in Windows you will need to place a copy in the /boot/ directory. You can do that using :

  sudo cp config.py /boot/vcu_config.py

When the cron.py script runs at boot time it will check if there is a vcu_config.py file in /boot/. If there is it copies it over config.py. The main script would then run and use the new settings.


Autorun On Boot Setup
---------------------

To run the script automatically when the Pi boots you can use 'cron'. To edit it we use the command :

  sudo crontab -e

Using your cursor keys scroll to the bottom and add the following line :

  @reboot python /home/pi/rpispy_vcu/cron.py &

Make sure you get this line correct as the script will fail to run on boot if there are any mistakes.

To save these changes click "CTRL-X", then "Y" and finally "Return". You should now be back at the command prompt.

To start testing you can now reboot using :

  sudo reboot